defmodule CzechStatsWeb.PageController do
  use CzechStatsWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
