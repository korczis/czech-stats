# This file is responsible for configuring your umbrella
# and **all applications** and their dependencies with the
# help of Mix.Config.
#
# Note that all applications in your umbrella share the
# same configuration and dependencies, which is why they
# all use the same configuration file. If you want different
# configurations or dependencies per app, it is best to
# move said applications out of the umbrella.
use Mix.Config



config :czech_stats_web,
  generators: [context_app: :czech_stats]

# Configures the endpoint
config :czech_stats_web, CzechStatsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "5qukOV5+T15kUOqCVMECWFaqNy7hE305YHJO9wnE8CMwNy6Pk10nfJzKSnSamjE5",
  render_errors: [view: CzechStatsWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: CzechStatsWeb.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
